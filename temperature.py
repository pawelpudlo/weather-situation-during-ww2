import plotly.graph_objects as go
import csv

namesList = []
recordList = []

with open('Summary of Weather.csv') as fileCSV:
    readerCSV = csv.reader(fileCSV)

    namesList = next(readerCSV)
    recordList = [wiersz for wiersz in readerCSV]

maxTemp = 0.0
minTemp = 0.0
y = 40
m = 1
d = 1
reduce = 0
date = ''
allList = []

stationList = []

while y < 46:

    m = 1

    while m < 13:
        d = 1
        while d < 32:

            for i in recordList:
                if y == 45 and ((m == 9 and d > 2) or (m > 9)):
                    continue
                if int(i.__getitem__(9)) == y and int(i.__getitem__(10)) == m and int(i.__getitem__(11)) == d:
                    maxTemp += float(i.__getitem__(4))
                    minTemp += float(i.__getitem__(5))
                    date = i.__getitem__(1)
                    reduce += 1
            if reduce != 0:
                krotka = date, maxTemp / reduce, minTemp / reduce
                allList.append(krotka)
            maxTemp = 0
            minTemp = 0
            reduce = 0
            d += 1
            if d > 31:
                continue
        m += 1
        if m > 12:
            continue
    y += 1

dateList = []
maxTempList = []
minTempList = []

for i in allList:
    dateList.append(i.__getitem__(0))
    maxTempList.append(i.__getitem__(1))
    minTempList.append(i.__getitem__(2))

figMax = go.Figure(data=go.Bar(x=dateList, y=maxTempList))
figMax.write_html('maxTemperature.html', auto_open=True)

figMin = go.Figure(data=go.Bar(x=dateList, y=minTempList))
figMin.write_html('minTemperature.html', auto_open=True)

'''
for i in allList:
    print(i)
'''
