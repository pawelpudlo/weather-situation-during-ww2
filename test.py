import pandas as pd
import plotly.graph_objects as go
import csv
import matplotlib.pyplot as plt

#df = pd.read_csv("Summary of Weather.csv", low_memory=False)

namesList = []
recordList = []

with open('Summary of Weather.csv') as fileCSV:
    readerCSV = csv.reader(fileCSV)

    namesList = next(readerCSV)

    recordList = [wiersz for wiersz in readerCSV]


numberOfStation = 0
numberStation = recordList.__getitem__(0).__getitem__(0)
stationList = []

for i in recordList:
    stationList.append(i.__getitem__(0))

# List have information about number station and number of Station occurrences in file cvs
numberOfStationOccurrencesList = []

for i in stationList:
    if numberStation == i:
        numberOfStation += 1
    else:
        numberOfStationOccurrencesList.append([numberStation, numberOfStation])
        numberStation = i
        numberOfStation = 1

numberStationList = []
occurrencesList = []

for i in numberOfStationOccurrencesList:
    numberStationList.append(i.__getitem__(0))
    occurrencesList.append(i.__getitem__(1))

fig = go.Figure(data=go.Bar(x=numberStationList,y=occurrencesList))
fig.write_html('occurrencesList.html', auto_open=True)
# print(dateList)

# print(df.head(10).get('MaxTemp'))


# df.describe()
# df.plot(x='Date', y='MaxTemp', style='o')
# plt.title('MinTemp vs MaxTemp')
# plt.xlabel('MinTemp')
# plt.ylabel('MaxTemp')
# plt.show()
# fig = go.Figure(data=go.Bar(y=[df]))
# fig.write_html('first_figure.html', auto_open=True)

